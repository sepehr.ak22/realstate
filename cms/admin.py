from django.contrib import admin
from .models import Site, Service, ContactFullName
from .submodels.service import Category
# Register your models here.

@admin.register(Site)
class SiteAdmin(admin.ModelAdmin):
    pass


@admin.register(ContactFullName)
class ContactAdmin(admin.ModelAdmin):
    pass


@admin.register(Service)
class ServiceAdmin(admin.ModelAdmin):
    pass


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    pass