from django.db import models

from .submodels import contact, service

from django.utils.translation import ugettext_lazy as _

from django.core.validators import (
    RegexValidator,
    MinLengthValidator,
    MaxLengthValidator,
    EmailValidator,
    FileExtensionValidator,
    URLValidator
)

from painless.utils.regex.patterns import PERSIAN_PHONE_NUMBER_PATTERN
from painless.utils.models.common import ImageModelMixin, VideoModelMixin
from painless.utils.upload.path import image_upload_to, date_directory_path

from .submodels.contact import ContactFullName
from .submodels.service import Service


class Site(ImageModelMixin, VideoModelMixin):
    domain = models.URLField(
        _("Site Domain"),
        validators=[
            URLValidator(),
        ],
    )

    site_name = models.CharField(
        _("Site Name"),
        max_length=30,
        validators=[
            MaxLengthValidator(30, message='خوش آمد گویی نمی تواند بیش تر از 30 کاراکتر باشد'),
            MinLengthValidator(5, message='خوش آمد گویی نمی تواند کمتر از 5 کاراکتر باشد')
        ],
        null=True,
        blank=True
    )

    phone_number = models.CharField(
        _("Phone Number"),
        max_length=12,
        validators=[
            RegexValidator(PERSIAN_PHONE_NUMBER_PATTERN,
                           message='please enter like these patterns `+989xxxxxxxxx`, `09xxxxxxxxx`.'),
            MinLengthValidator(11),
            MaxLengthValidator(13)
        ],
    )

    address = models.CharField(_("Address"), max_length=200)

    welcome_title = models.CharField(
        _("Welcome Title"),
        max_length=50,
        validators=[
            MaxLengthValidator(50, message='خوش آمد گویی نمی تواند بیش تر از 50 کاراکتر باشد'),
            MinLengthValidator(10, message='خوش آمد گویی نمی تواند کمتر از 10 کاراکتر باشد')
        ],
        null=True,
        blank=True
    )
    
    welcome_summary = models.TextField(
        _("Welcome Summary"),
        max_length=250,
        validators=[
            MaxLengthValidator(250, message='خوش آمد گویی نمی تواند بیش تر از 250 کاراکتر باشد'),
            MinLengthValidator(20, message='خوش آمد گویی نمی تواند کمتر از 20 کاراکتر باشد')
        ],
        null=True,
        blank=True
    )

    about_us_title = models.CharField(
        _("AboutUs Title"),
        max_length=40,
        validators=[
            MaxLengthValidator(40, message='خوش آمد گویی نمی تواند بیش تر از 40 کاراکتر باشد'),
            MinLengthValidator(8, message='خوش آمد گویی نمی تواند کمتر از 8 کاراکتر باشد')
        ],
        null=True,
        blank=True,
    )

    about_us = models.TextField(_("About Us"))

    about_us_pic = models.ImageField(
        _("about Us Picture"),
        upload_to=date_directory_path,
        height_field='width_field_about',
        width_field='height_field_about',
        max_length=110,
        validators=[FileExtensionValidator(allowed_extensions=['JPG', 'JPEG', 'PNG', 'jpg', 'jpeg', 'png'])]
    )

    width_field_about = models.PositiveSmallIntegerField(_("Width Field"), editable=False)
    height_field_about = models.PositiveSmallIntegerField(_("Height Field"), editable=False)

    about_us_alternate_text = models.CharField(
        _("Alternate Text"),
        max_length=110,
        validators=[
            MaxLengthValidator(150),
            MinLengthValidator(3)
        ]
    )

    footer_text = models.TextField()

    class Meta:
        verbose_name = 'Site'
        verbose_name_plural = 'Sites'

    def __str__(self):
        return self.domain

    def save(self, *args, **kwargs):
        super(Site, self).save(*args, **kwargs)


class RequestInquiry(models.Model):
    full_name = models.CharField(_("Address"), max_length=200)
    email = models.EmailField(_("Email"), validators=[EmailValidator], max_length=254)
    message = models.TextField(_("Message"))

    phone_number = models.CharField(
        _("Phone Number"),
        max_length=12,
        validators=[
            RegexValidator(PERSIAN_PHONE_NUMBER_PATTERN,
                           message='please enter like these patterns `+989xxxxxxxxx`, `09xxxxxxxxx`.'),
            MinLengthValidator(11),
            MaxLengthValidator(13)
        ],
    )

    class Meta:
        verbose_name = _('Request Inquiry')
        verbose_name_plural = _('Request Inquiries')

    def __str__(self):
        return self.full_name

    def save(self, *args, **kwargs):
        super(RequestInquiry, self).save(*args, **kwargs)


class Page(ImageModelMixin):
    title = models.CharField(_("Title"), max_length=100)
