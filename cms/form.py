from django import forms

from .models import (
    Contact,
    RequestInquiry
)

class ContactForm(forms.ModelForm):
    
    class Meta:
        model = Contact
        fields = ('full_name' ,'email' ,'message' ,'phone_number')

class RequestInquiryForm(forms.ModelForm):

    class Meta:
        model = RequestInquiry
        fields = ('full_name' ,'email' ,'message' ,'phone_number')
