import secrets

from django.db import models
from django.utils.text import slugify
from django.utils.translation import ugettext_lazy as _

from django.urls import reverse

from django.core.validators import (
    FileExtensionValidator,
    RegexValidator
)

from painless.utils.upload.path import date_directory_path

from painless.utils.models.common import (
    TimeStampModelMixin,
    ImageModelMixin,
    TitleSlugLinkModelMixin
)


class Service(TitleSlugLinkModelMixin, ImageModelMixin, TimeStampModelMixin):
    sku = models.CharField(_("SKU"), editable=False, max_length=7)
    description = models.TextField(_("Description"))
    category = models.ForeignKey('Category', related_name='services', on_delete=models.CASCADE)

    def save(self, *args, **kwargs):
        self.sku = secrets.token_hex(3).upper()
        self.slug = slugify(self.title, allow_unicode=True)
        super(Service, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('cms:service', kwargs={'slug': self.slug})

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = _('Service')
        verbose_name_plural = _('Services')


class Category(TitleSlugLinkModelMixin, TimeStampModelMixin):
    class Meta:
        verbose_name = _('Category')
        verbose_name_plural = _('Categories')

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title, allow_unicode=True)
        super(Category, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('cms:service_category', kwargs={'slug': self.slug})

    def __str__(self):
        return self.title
