from django import forms

from .models import (
    Amenities,
    Property,
    Category,
    Tag,
    Feature,
    Gallery,
    Detail
)

class PropertyForm(forms.ModelForm):
    class Meta:
        model = Property
        fields = ['title', 'price', 'is_featured', 'status', 'kind', 'area', 'description', 'floorplan', 'alternate_text_floorplan', 'width_field_floorplan', 'height_field_floorplan', 'category',]

class CategoryForm(forms.ModelForm):
    class Meta:
        model = Category
        fields = ['title',]

class TagForm(forms.ModelForm):
    class Meta:
        model = Tag
        fields = ['title', 'item']

class FeatureForm(forms.ModelForm):
    class Meta:
        model = Feature
        fields = ['title', 'item']

class GalleryForm(forms.ModelForm):
    class Meta:
        model = Gallery
        fields = ['item', 'picture', 'alternate_text',]

class AmenityForm(forms.ModelForm):
    class Meta:
        model = Amenities
        fields = ['key', 'value', 'item']

class Detail(forms.ModelForm):
    class Meta:
        model = Detail
        fields = ['key', 'value', 'item']