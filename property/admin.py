from django.contrib import admin

from .models import (
    Amenities,
    Property,
    Category,
    Tag,
    Feature,
    Gallery,
    Detail,
    Icon
)


@admin.register(Property)
class PropertyAdmin(admin.ModelAdmin):
    '''Admin View for Property'''

    list_display = ['title', 'price', 'is_featured', 'status', 'kind', 'area', 'category', 'created', 'modified']
    list_filter = ('is_featured', 'status', 'kind', 'category', 'created', 'modified')
    search_fields = ('title', 'price',)
    ordering = ('-created',)

@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    '''Admin View for Category'''

    list_display = ('title', 'created', 'modified')
    list_filter = ('created', 'modified')
    search_fields = ('title',)
    ordering = ('-created',)

@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    '''Admin View for Tag'''

    list_display = ('title', 'created', 'modified')
    list_filter = ('created', 'modified')
    search_fields = ('title',)
    ordering = ('-created',)

@admin.register(Feature)
class FeatureAdmin(admin.ModelAdmin):
    '''Admin View for Feature'''

    list_display = ('title', 'item', 'icon', 'created', 'modified')
    list_filter = ('created', 'modified')
    search_fields = ('title',)
    ordering = ('-created',)

@admin.register(Gallery)
class GalleryAdmin(admin.ModelAdmin):
    '''Admin View for Gallery'''

    list_display = ('item', 'picture_alternate_text', 'created', 'modified')
    list_filter = ('created', 'modified')
    search_fields = ('picture_alternate_text',)
    ordering = ('-created',)

@admin.register(Detail)
class DetailAdmin(admin.ModelAdmin):
    '''Admin View for Detail'''

    list_display = ('key', 'value', 'item', 'created', 'modified',)
    list_filter = ('created', 'modified',)
    search_fields = ('key',)
    ordering = ('-created',)

@admin.register(Amenities)
class AmenitiesAdmin(admin.ModelAdmin):
    '''Admin View for Amenities'''

    list_display = ('key', 'value', 'item', 'created', 'modified',)
    list_filter = ('created', 'modified',)
    search_fields = ('key',)
    ordering = ('-created',)


@admin.register(Icon)
class IconAdmin(admin.ModelAdmin):
    pass
