import secrets

from django.db import models
from django.utils.text import slugify
from django.utils.translation import ugettext_lazy as _

from django.urls import reverse

from django.core.validators import (
    FileExtensionValidator,
    RegexValidator
)

from painless.utils.upload.path import date_directory_path

from painless.utils.models.common import (
    TimeStampModelMixin,
    ImageModelMixin,
    TitleSlugLinkModelMixin
)


class Property(TitleSlugLinkModelMixin, ImageModelMixin, TimeStampModelMixin):
    STATUS = (
        ('r', _('Rent')),
        ('b', _('Buy'))
    )

    KIND = (
        ('a', _('apartment')),
        ('h', _('house')),
        ('g', _('ground'))
    )

    sku = models.CharField(_("SKU"), editable=False, max_length=7)

    price = models.CharField(
        _("Price"),
        max_length=16,
        validators=[
            RegexValidator(r'\d+', message=_('price must be digit.')),
        ]
    )
    is_featured = models.BooleanField(_("Is Featured"), default=False)
    is_popular = models.BooleanField(_("Is Popular"), default=False)

    status = models.CharField(_("Status"), choices=STATUS, max_length=1)
    kind = models.CharField(_("Kind"), choices=KIND, max_length=1)
    area = models.CharField(
        _("Area"),
        max_length=12,
        validators=[RegexValidator(r'\d+x\d+',
                                   message=_("Please enter area in this format `(100x200)`. TIP: (width, height) "))]
    )

    description = models.TextField(_("Description"))

    category = models.ForeignKey('Category', related_name='properties', on_delete=models.PROTECT)

    def save(self, *args, **kwargs):
        self.sku = secrets.token_hex(3).upper()
        self.slug = slugify(self.title, allow_unicode=True)
        super(Property, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('property', kwargs={'slug': self.slug})

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = _('Property')
        verbose_name_plural = _('Properties')


class Category(TitleSlugLinkModelMixin, TimeStampModelMixin):
    class Meta:
        verbose_name = _('Category')
        verbose_name_plural = _('Categories')

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title, allow_unicode=True)
        super(Category, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('property_category', kwargs={'slug': self.slug})

    def __str__(self):
        return self.title


class Tag(TitleSlugLinkModelMixin, TimeStampModelMixin):
    item = models.ManyToManyField("Property", db_column='property', related_name='tag', verbose_name=_("Tag Property"))

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title, allow_unicode=True)
        super(Tag, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('property_tag', kwargs={'slug': self.slug})

    def __str__(self):
        return self.title


class Feature(TitleSlugLinkModelMixin, TimeStampModelMixin):
    item = models.ForeignKey(
        Property,
        db_column='property',
        verbose_name=_("Feature"),
        related_name='features',
        on_delete=models.CASCADE
    )

    icon = models.ForeignKey(
        'Icon',
        db_column='icon',
        verbose_name=_("Icon"),
        related_name='features',
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title, allow_unicode=True)
        super(Feature, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('property_feature', kwargs={'slug': self.slug})

    def __str__(self):
        return self.title


class Gallery(TimeStampModelMixin, ImageModelMixin):
    item = models.ForeignKey(Property, db_column='property', verbose_name=_("Feature"), related_name='pictures',
                             on_delete=models.CASCADE)

    def __str__(self):
        return self.alternate_text

    def get_absolute_url(self):
        return reverse('property_gallery', kwargs={'slug': self.slug})

    class Meta:
        verbose_name = _('Gallery')
        verbose_name_plural = _('Galleries')


class Amenities(TimeStampModelMixin):
    key = models.CharField(_("Key"), max_length=150)
    value = models.BooleanField(_("Value"))
    item = models.ForeignKey(Property, db_column='property', verbose_name=_("Amenities"), related_name='ameneties',
                             on_delete=models.CASCADE)

    def get_absolute_url(self):
        return reverse('property_aminity', kwargs={'slug': self.slug})

    def __str__(self):
        return self.key


class Detail(TimeStampModelMixin):
    key = models.CharField(_("Key"), max_length=150)
    value = models.CharField(_("Key"), max_length=150)
    item = models.ForeignKey(Property, db_column='property', verbose_name=_("Detail"), related_name='details',
                             on_delete=models.CASCADE)

    def get_absolute_url(self):
        return reverse('property_detail', kwargs={'slug': self.slug})

    def __str__(self):
        return self.key


class Icon(models.Model):
    name = models.CharField(
        max_length=30,
        verbose_name=_("Name"),
    )

    code = models.CharField(
        _("Code"),
        max_length=30,
    )
    
    def __str__(self):
        return self.name
