from django.db import models

from django.core.validators import (
    FileExtensionValidator,
    MinLengthValidator,
    MaxLengthValidator
)

from django.utils.translation import ugettext_lazy as _

from painless.utils.upload.path import date_directory_path


class TitleSlugLinkModelMixin(models.Model):
    title = models.CharField(
        _("Title"),
        max_length=150,
        validators=[
            MaxLengthValidator(150),
            MinLengthValidator(3)
        ],
        unique=True
    )
    slug = models.SlugField(
        _("Slug"),
        editable=False,
        allow_unicode=True,
        max_length=150,
        unique=True
    )

    class Meta:
        abstract = True


class TimeStampModelMixin(models.Model):
    created = models.DateTimeField(_("Created"), auto_now_add=True)
    modified = models.DateTimeField(_("Modified"), auto_now=True)

    class Meta:
        abstract = True


class ImageModelMixin(models.Model):
    picture = models.ImageField(
        _("Picture"),
        upload_to=date_directory_path,
        height_field='height_field',
        width_field='width_field',
        max_length=110,
        validators=[FileExtensionValidator(allowed_extensions=['JPG', 'JPEG', 'PNG', 'jpg', 'jpeg', 'png'])]
    )

    picture_alternate_text = models.CharField(
        _("Picture Alternate Text"),
        max_length=110,
        validators=[
            MaxLengthValidator(150),
            MinLengthValidator(3)
        ]
    )
    width_field = models.PositiveSmallIntegerField(_("Width Field"), editable=False)
    height_field = models.PositiveSmallIntegerField(_("Height Field"), editable=False)

    class Meta:
        abstract = True


class VideoModelMixin(models.Model):
    video = models.FileField(
        _("Video"),
        upload_to=date_directory_path,
        max_length=110,
        validators=[
            FileExtensionValidator(allowed_extensions=['mp4', ])
        ]
    )

    video_alternate_text = models.CharField(
        _("Video Alternate Text"),
        max_length=110,
        validators=[
            MaxLengthValidator(150),
            MinLengthValidator(3)
        ]
    )

    class Meta:
        abstract = True
