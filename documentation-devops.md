<!-- Database Creation Help -->

CREATE USER postgres WITH PASSWORD '1234';
CREATE DATABASE gilda;
ALTER ROLE postgres SET client_encoding TO 'utf8';
ALTER ROLE postgres SET default_transaction_isolation TO 'read committed';
ALTER ROLE postgres SET timezone TO 'UTC';
GRANT ALL PRIVILEGES ON DATABASE gilda TO postgres;


<!-- Multi Language Command -->
python manage.py makemessages --ignore="static" --ignore="venv" -l qa


<!-- Django Admin Interface -->
python manage.py loaddata admin_interface_theme_uswds.json
>>> leltajmil\venv\Lib\site-packages\django\contrib\admin\templates\admin\base_site.html
{% extends "admin_interface:admin/base_site.html" %}

<!-- Django Migrations -->
python manage.py makemigrations
python manage.py migrate

<!-- Django Deployment Check -->
python manage.py collectstatic
python manage.py check
python manage.py test
python manage.py check --deploy

<!-- Multi Language Command -->

***** Dont use these commands until development team tell devops engineer.
<!-- python manage.py makemessages --ignore="static" --ignore="venv" -l ar -->
<!-- python manage.py compilemessages -->

<!-- package management -->
## if rcssmin or jsmin in django compressor doesnt install
pip install rcssmin --install-option="--without-c-extensions"
pip install rjsmin --install-option="--without-c-extensions"
pip install django-compressor --upgrade

## if psycopg2 doesnt install on ubuntu or linux
pip install psycopg2-binary

## django-admin-honeypot
Install django-admin-honeypot from PyPI:

pip install django-admin-honeypot
Add admin_honeypot to INSTALLED_APPS

Update your urls.py:

urlpatterns = [
    ...
    path('admin/', include('admin_honeypot.urls', namespace='admin_honeypot')),
    path('secret/', admin.site.urls),
]


