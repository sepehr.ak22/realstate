from django.shortcuts import render
from django.utils.translation import ugettext_lazy as _
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse_lazy

from django.views.generic import (
    TemplateView,
    CreateView,
    DetailView
)

from property.models import Property

from cms.models import Site
from cms.submodels.service import Category, Service


class HomeView(TemplateView):
    template_name = "pages/index.html"
    page_name = _("خانه")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['site'] = Site.objects.first()
        context['properties'] = Property.objects.filter(is_featured=True).order_by('-created')[:3]
        context['popularـproperties'] = Property.objects.filter(is_popular=True).order_by('-created')[:4]
        context['services'] = Service.objects.all().order_by('-created')[:4]
        return context


class ContactView(TemplateView):
    template_name = "pages/contact-us.html"
    page_name = _("تماس باما")

    def get_context_data(self, **kwargs):
        site = Site.objects.first()
        context = super().get_context_data(**kwargs)
        context['site'] = site
        return context


class AboutView(TemplateView):
    template_name = "pages/about-us.html"
    page_name = _("درباره ما")

    def get_context_data(self, **kwargs):
        site = Site.objects.first()
        context = super().get_context_data(**kwargs)
        context['site'] = site
        context['services'] = Service.objects.all().order_by('-created')[:3]
        return context


class PropertyListView(TemplateView):
    template_name = "pages/properties.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context


class VideoBannerPartialView(TemplateView):
    template_name = 'partials/video-banner.html'
    page_name = _("بنر")

    def get_context_data(self, **kwargs):

        kwargs['site'] = Site.objects.first()

        return super(VideoBannerPartialView, self).get_context_data(**kwargs)


class Navigation(TemplateView):
    template_name = 'partials/nav.html'

    def get_context_data(self, **kwargs):
        return super(Navigation, self).get_context_data(**kwargs)

# class TopBanner(TemplateView):
#     template_name = 'partials/top-banner.html'
#
#     def get_context_data(self, **kwargs):
#         return super(TopBanner, self).get_context_data(**kwargs)
