from django.contrib import admin

from django.views.decorators.cache import cache_page

from django.urls import (
    path,
    re_path,
    include
)

from . import views

app_name = 'pages'

urlpatterns = [

    re_path(
        r'^$',
        views.HomeView.as_view(),
        name='home'),

    re_path(
        r'^contact/$',
        views.ContactView.as_view(),
        name='contact'),

    re_path(
        r'^about/$',
        views.AboutView.as_view(),
        name='about'),

    re_path(
        r'^properties/$',
        views.PropertyListView.as_view(),
        name='properties'),

    re_path(
        r'^video-banner/$',
        views.VideoBannerPartialView.as_view(),
        name='video-banner'),

    # re_path(
    #     r'^top-banner/(?P<page_name>\w+)/$',
    #     views.TopBanner.as_view(),
    #     name='top-banner'),
]
