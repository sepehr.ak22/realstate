from .base import INSTALLED_APPS


# ############## #
#   EXTENSIONS   #
# ############## #
INSTALLED_APPS.append('painless')
INSTALLED_APPS.append('django_render_partial', )
INSTALLED_APPS.append('admin_honeypot', )
INSTALLED_APPS.append('admin_interface',)
INSTALLED_APPS.append('colorfield',)


# ############## #
# CUSTOM PROJECT #
# ############## #
INSTALLED_APPS.append('property')
INSTALLED_APPS.append('cms')


# ############################# #
# CONFIG DJANGO ADMIN INTERFACE #
# ############################# #
X_FRAME_OPTIONS='SAMEORIGIN'